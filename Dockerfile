FROM alpine:3.14.0
LABEL maintainer="tak.aoki.jp@gmail.com"
 
ENV LANG=ja_JP.UTF-8 JP_PORT=8888 JP_USER=gitlab
 
#EXPOSE ${JP_PORT}

RUN apk update && apk upgrade && apk add --no-cache \
        curl \
        jq
